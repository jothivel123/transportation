from calendar import HTMLCalendar

class CustomHTMLCalendar(HTMLCalendar):
	
    # CSS classes for the day <td>s
    cssclasses = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]
	
    def __init__(self, entityObject):
        super(CustomHTMLCalendar, self).__init__()
        self.entityObject = entityObject

    def formatday(self, date_row):
        """
        Return a day as a table cell.
        """
        checked = ''
        availabilityMonth = []
        
        if bool(self.entityObject):
            availability = self.entityObject['availability']
            #print(availability)
            availabilityYear = availability[str(date_row.year)]
            #print(availabilityYear)
            #print(date_row.month)
            if str(date_row.month) in availabilityYear:
                #print("INSIDE IF")
                availabilityMonth = availabilityYear[str(date_row.month)]
                #print(availabilityMonth)
            if date_row.day in availabilityMonth:
                checked = 'checked="checked" '		
		
        if date_row.month != self.month:
            return '<td class="noday">&nbsp;</td>' # day outside month
        else:
			#str(date_row.day) + "_" + str(date_row.year), date_row.day)
            #return '<td class="%s"><input type="checkbox" name="date_availability" %svalue="%s_%s_%s">%d <br /><span>O.T</span><input type="text" name="ot_%s" value=""><br /><span>C.T</span><input type="text" name="ct_%s" value=""></td>' % (self.cssclasses[date_row.weekday()], checked, date_row.day, date_row.month, date_row.year, date_row.day, str(date_row.day)+"_"+str(date_row.month)+"_"+str(date_row.year), str(date_row.day)+"_"+str(date_row.month)+"_"+str(date_row.year))
            return '<td class="%s"><input type="checkbox" name="date_availability" %svalue="%s_%s_%s">%d</td>' % (self.cssclasses[date_row.weekday()], checked, date_row.day, date_row.month, date_row.year, date_row.day)
 
    def formatweek(self, theweek):
        """
        Return a complete week as a table row.
        """
        s = ''.join(self.formatday(date_row) for date_row in theweek)
        return '<tr>%s</tr>' % s
 
    def formatmonth(self, theyear, themonth, withyear=True):
        """
        Return a formatted month as a table.
        """
        v = []
        a = v.append
        a('<table border="0" cellpadding="0" cellspacing="0" class="month">')
        a('\n')
        a(self.formatmonthname(theyear, themonth, withyear=withyear))
        a('\n')
        a(self.formatweekheader())
        a('\n')
        dates = list(self.itermonthdates(theyear, themonth))
        self.month = themonth
        records = [ dates[i:i+7] for i in range(0, len(dates), 7) ]
        for week in records:
            a(self.formatweek(week))
            a('\n')
        a('</table>')
        a('\n')
        return ''.join(v)
 
    def formatyear(self, theyear, width=3):
        """
        Return a formatted year as a table of tables.
        """
        v = []
        a = v.append
        width = max(width, 1)
        a('<table border="0" cellpadding="0" cellspacing="0" class="year">')
        a('\n')
        a('<tr><th colspan="%d" class="year">%s</th></tr>' % (width, theyear))
        for i in range(January, January+12, width):
            # months in this row
            months = range(i, min(i+width, 13))
            a('<tr>')
            for m in months:
                a('<td>')
                a(self.formatmonth(theyear, m, withyear=False))
                a('</td>')
            a('</tr>')
        a('</table>')
        return ''.join(v)
 
 
