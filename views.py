from flask import Flask, request, render_template, redirect, url_for
from models import Facility
from flask_mongoengine.wtf import model_form
from werkzeug.datastructures import MultiDict
from flask_pymongo import PyMongo

mongo = PyMongo()


def index():
	#facilityList = Facility.objects.all()
	facilityList = mongo.db.testdb.find({})
	#return "Hello World!"
	#form = model_form(Facility, field_args = {'name': {'label': 'Name'}, 'address':  {'label': 'Address'}, 'phonenumber':  {'label': 'Phone Number'}})(request.form)
	return render_template('index.html', facilityList=facilityList)

def admin_add():
	#online_users = mongo.db.users.find({})
	#return "Hello World!"
	return render_template('admin_add.html')
	

def facility_add():
	#online_users = mongo.db.users.find({})
	#return "Hello World!"
	return render_template('facility_add.html')


def facility_add_action():
   facilityName = request.form['name']
   facilityAddress = request.form['address']
   facilityPhoneNumber = request.form['phonenumber']
   #print ("facilityName {}".format(facilityName))
   #print ("facilityAddress {}".format(facilityAddress))
   #print ("facilityPhoneNumber {0}".format(facilityPhoneNumber))
   #sys.exit(0)
   #facility = 
   facility = {'name': facilityName, 'address': facilityAddress, 'phonenumber': facilityPhoneNumber}
   #print (request.form)
   
   FacilityForm = model_form(Facility)
   form = FacilityForm(MultiDict(facility))
   form.save()
   
   #request.form
   #facility.name = facilityName
   #facility.address = facilityAddress
   #facility.phonenumber = facilityPhoneNumber   
   #mongo.db.facility.insert_one(facility)
   return redirect(url_for('index'))
	

def admin_form():
   location = request.form['location']
   mongo.db.testdb.insert_one({'location': location})
   return render_template('admin_add.html')