from flask import Flask, request, render_template, redirect, url_for
from flask_pymongo import PyMongo
from bson.json_util import dumps
import sys
import re, datetime, calendar
from CustomHTMLCalendar import CustomHTMLCalendar

app = Flask(__name__)

app.config['MONGO_HOST'] = '35.187.0.90'
app.config['MONGO_PORT'] = '27017'
app.config['MONGO_DBNAME'] = 'admin'
app.config['MONGO_USERNAME'] = 'root'
app.config['MONGO_PASSWORD'] = '6a7WAfZ1'

#Google Map API Key
app.config['GOOGLEMAPS_KEY'] = "AIzaSyBgyhKX_x5nR3LXiDGKJg3OiyBAlsdbKaw"

mongo = PyMongo(app)

def getNextSequence(name):
	sequenceDocument  = mongo.db.counters.find_and_modify(
        query = { '_id': name },
        update = {'$inc': {'sequence_value': 1}},
        new = True 
	).get('sequence_value')
	return sequenceDocument;
	
@app.route('/getFacility/<int:client_id>')
def getFacility(client_id):
	str = request.args.get('term')
	clients_facility_map = mongo.db.clients_facility_map.find({"cid": client_id}, {"_id": 0, "fid": 1})
	fids = []
	for cfm in clients_facility_map:
		fids.insert(0, cfm['fid'])
		
	facilityList = mongo.db.facility.find({"$and": [{"name" : {'$regex' : ".*" + str + ".*", '$options' : 'i'}}, {"id": { "$nin": fids }}]}, {"id": 1, "name": 1, "_id": 0})	
	return render_template('getFacility.html', facilityList=dumps(facilityList))

@app.route('/getAllFacility')
def getAllFacility():
	str = request.args.get('term')
	facilityList = mongo.db.facility.find({"$and": [{"name" : {'$regex' : ".*" + str + ".*", '$options' : 'i'}}]}, {"id": 1, "name": 1, "_id": 0})
	return render_template('getAllFacility.html', facilityList=dumps(facilityList))

@app.route("/")
def index():
	return render_template('index.html')
	
@app.route("/facility")
def facility():
	facilityList = mongo.db.facility.find({})
	return render_template('facility.html', facilityList=facilityList)

@app.route("/facility/add")
def facility_add():
	facility=[]
	myCal = CustomHTMLCalendar(facility)
	now = datetime.datetime.now()
	#print now.year, now.month, now.day, now.hour, now.minute, now.second
	calendarDisplay = myCal.formatmonth(now.year, now.month)
	return render_template('facility_add.html', facility=facility, calendarDisplay=calendarDisplay)

@app.route('/facility/add', methods = ['POST'])
def facility_add_action():
	date_availability = request.form.getlist('date_availability')
	#newAvailability = []
	yearsDict = {}
	monthDict = {}
	dayList = []
	for entries in date_availability:
		splitted = entries.split('_')
		yearValue = splitted[2]
		monthValue = splitted[1]
		dayValue = int(splitted[0])
		
		dayList.append(dayValue)
		
		if yearValue in yearsDict:			
			if monthValue in monthDict:				
				monthDict[monthValue] = dayList				
			else:
				monthDict[monthValue] = dayList
				
			yearsDict[yearValue].update(monthDict)
		else:
			if monthValue in monthDict:
				monthDict[monthValue] = dayList
			else:
				monthDict[monthValue] = dayList

			yearsDict[yearValue] = monthDict
		
	facilityName = request.form['name']
	facilityAddress = request.form['address']
	facilityPhoneNumber = request.form['phonenumber']
	availability = request.form.getlist('availability')

	facility = {'id': getNextSequence('facility'), 'name': facilityName, 'address': facilityAddress, 'phonenumber': facilityPhoneNumber, 'availability': yearsDict}
   
	mongo.db.facility.insert_one(facility)
	return redirect(url_for('facility'))

@app.route("/facility/edit/<int:id>")
def facility_edit(id):
	facility = mongo.db.facility.find_one({'id': id})
	
	myCal = CustomHTMLCalendar(facility)
	now = datetime.datetime.now()
	#print now.year, now.month, now.day, now.hour, now.minute, now.second
	calendarDisplay = myCal.formatmonth(now.year, now.month)
	
	return render_template('facility_add.html', facility=facility, calendarDisplay=calendarDisplay)	

@app.route('/facility/edit/<int:id>', methods = ['POST'])
def facility_edit_action(id):
	facilityOld = mongo.db.facility.find_one({'id': id})	
	date_availability = request.form.getlist('date_availability')
	#newAvailability = []
	yearsDict = {}
	monthDict = {}
	dayList = []
	#print(date_availability)
	for entries in date_availability:
		splitted = entries.split('_')
		yearValue = splitted[2]
		monthValue = splitted[1]
		dayValue = int(splitted[0])
		
		dayList.append(dayValue)
		
		if yearValue in yearsDict:			
			if monthValue in monthDict:				
				monthDict[monthValue] = dayList				
			else:
				monthDict[monthValue] = dayList
				
			yearsDict[yearValue].update(monthDict)
		else:
			if monthValue in monthDict:
				monthDict[monthValue] = dayList
			else:
				monthDict[monthValue] = dayList

			yearsDict[yearValue] = monthDict
	
	#print(yearsDict)
	oldAvailability = facilityOld['availability']
	newAvailability = oldAvailability.copy()
	#print("##### New Availability before update ##########")
	#print(newAvailability)
	if bool(yearsDict):
		for years in yearsDict:
			if(years in newAvailability):
				#print(years)
				#print(yearsDict[years])
				newMonthDict1 = yearsDict[years]
				newMonthDict2 = newAvailability[years]
				for months in newMonthDict1:
					if(months in newMonthDict2):
						#print(months)
						#print(newMonthDict1[months])
						newAvailability[years][months] = newMonthDict1[months]
					else:
						del newAvailability[years][months]
			else:
				del newAvailability[years]
	else:
		newAvailability = {}
		
	
	
	
	
	#print("##### OLD Availability ##########")
	#print(oldAvailability)
	#print("##### Availability ##########")
	#print(yearsDict)
	
	#newAvailability = yearsDict.copy()
	#print("##### New Availability before update ##########")
	#print(newAvailability)
	#newAvailability.update(oldAvailability)
	
	#print("##### New Availability after update ##########")
	#print(newAvailability)
	
	#sys.exit(0)
	
	facilityName = request.form['name']
	facilityAddress = request.form['address']
	facilityPhoneNumber = request.form['phonenumber']
	availability = request.form.getlist('availability')
	facility = {'id': id, 'name': facilityName, 'phonenumber': facilityPhoneNumber, 'availability': newAvailability}
	if facilityAddress:
		facility.update({'address': facilityAddress})
	else:
		facility.update({'address': facilityOld['address']})
		
	mongo.db.facility.replace_one({'id': id}, facility)
	return redirect(url_for('facility'))
	
@app.route("/facility/delete/<int:id>")
def facility_delete(id):
	facility = mongo.db.facility.delete_one({'id': id})
	return redirect(url_for('facility'))
	
@app.route("/client")
def client():
	clientList = mongo.db.clients.find({})
	return render_template('client.html', clientList=clientList)

@app.route("/client/add")
def client_add():
	return render_template('client_add.html', client=[])

@app.route('/client/add', methods = ['POST'])
def client_add_action():

	facilityClientId = request.form['facility_client_id']
	
	clientLastName = request.form['lastname']
	clientMiddleName = request.form['middlename']
	clientFirstName = request.form['firstname']
	
	clientAddress = request.form['address']
	
	clientContactNumber = request.form['contactnumber']
	clientSecondaryNumber = request.form['secondarynumber']
	clientAnnouncementNumber = request.form['announcementnumber']
	
	client = {
		'id': getNextSequence('clients'), 
		'facility_client_id': facilityClientId, 
		'lastname': clientLastName, 
		'middlename': clientMiddleName, 
		'firstname': clientFirstName, 
		'address': clientAddress,
		'contactnumber': clientContactNumber,
		'secondarynumber': clientSecondaryNumber,
		'announcementnumber': clientAnnouncementNumber
	}
   
	mongo.db.clients.insert_one(client)
	return redirect(url_for('client'))

@app.route("/client/edit/<int:id>")
def client_edit(id):
	client = mongo.db.clients.find_one({'id': id})
	return render_template('client_add.html', client=client)	

@app.route('/client/edit/<int:id>', methods = ['POST'])
def client_edit_action(id):
	clientOld = mongo.db.clients.find_one({'id': id})	
	
	facilityClientId = request.form['facility_client_id']
	
	clientLastName = request.form['lastname']
	clientMiddleName = request.form['middlename']
	clientFirstName = request.form['firstname']
	
	clientAddress = request.form['address']
	
	clientContactNumber = request.form['contactnumber']
	clientSecondaryNumber = request.form['secondarynumber']
	clientAnnouncementNumber = request.form['announcementnumber']
	
	client = {
		'id': getNextSequence('clients'), 
		'facility_client_id': facilityClientId, 
		'lastname': clientLastName, 
		'middlename': clientMiddleName, 
		'firstname': clientFirstName,
		'contactnumber': clientContactNumber,
		'secondarynumber': clientSecondaryNumber,
		'announcementnumber': clientAnnouncementNumber
	}
	
	if clientAddress:
		client.update({'address': clientAddress})
	else:
		client.update({'address': clientOld['address']})
		
	mongo.db.clients.replace_one({'id': id}, client)
	return redirect(url_for('client'))

@app.route("/client/delete/<int:id>")
def client_delete(id):
	client = mongo.db.clients.delete_one({'id': id})
	return redirect(url_for('client'))

@app.route("/client/facility/registration/<int:client_id>")
def client_facility_registration(client_id):
	client = mongo.db.clients.find_one({'id': client_id})
	clients_facility_map = mongo.db.clients_facility_map.find({"cid": client_id})
	fids = []
	for cfm in clients_facility_map:
		fids.insert(0, cfm['fid'])
	
	facilityList = mongo.db.facility.find({"id": { "$in": fids }})
	return render_template('client_facility_registration.html', client=client, clients_facility_map=clients_facility_map, facilityList=facilityList)	
	
@app.route("/client/facility/registration/<int:client_id>", methods = ['POST'])
def client_facility_registration_action(client_id):
	clientId = request.form['client_id']	
	facilityId = request.form['facility_id']
	transportationToFacility = request.form['trans_to_fac']
	transportationFromFacility = request.form['trans_from_fac']
	spTransNts = request.form['sp_trans_nts']
	
	client_facility_registration = {
		'cid': int(clientId), 
		'fid': int(facilityId),
		'trans_to_fac': int(transportationToFacility),
		'trans_from_fac': int(transportationFromFacility),
		'sp_trans_nts': spTransNts,
	}
	
	mongo.db.clients_facility_map.insert_one(client_facility_registration)	
	return redirect(url_for('client_facility_registration', client_id=clientId ))

@app.route("/client/facility/remove/<int:client_id>/<int:facility_id>")
def client_facility_remove(client_id, facility_id):
	client = mongo.db.clients_facility_map.delete_one({"$and": [{"cid": client_id}, {"fid": facility_id}]})
	return redirect(url_for('client_facility_registration', client_id=client_id ))
	
@app.route("/adminusers")
def adminusers():
	adminusersList = mongo.db.adminusers.find({})
	return render_template('adminusers.html', adminusersList=adminusersList)

@app.route("/adminusers/add")
def adminusers_add():
	adminuser=[]
	return render_template('adminusers_add.html', adminuser=adminuser)

@app.route('/adminusers/add', methods = ['POST'])
def adminusers_add_action():
	userFirstName = request.form['firstname']
	userLastName = request.form['lastname']
	userPassword = request.form['password']
	userNickName = request.form['nickname']

	facility = {'id': getNextSequence('adminusers'), 'firstname': userFirstName, 'lastname': userLastName, 'password': userPassword, 'nickname': userNickName}
   
	mongo.db.adminusers.insert_one(facility)
	return redirect(url_for('adminusers'))

@app.route("/adminusers/edit/<int:id>")
def adminusers_edit(id):
	adminuser = mongo.db.adminusers.find_one({'id': id})
	return render_template('adminusers_add.html', adminuser=adminuser)	

@app.route('/adminusers/edit/<int:id>', methods = ['POST'])
def adminusers_edit_action(id):
	adminuserOld = mongo.db.adminusers.find_one({'id': id})	
	
	userFirstName = request.form['firstname']
	userLastName = request.form['lastname']
	userPassword = request.form['password']
	userNickName = request.form['nickname']
	
	#if adminuserOld['password'] != request.form['password']:
	#	userPassword = request.form['password']
	#else:
	#	userPassword = adminuserOld['password']
	
	adminuser = {'id': id, 'firstname': userFirstName, 'lastname': userLastName, 'password': userPassword, 'nickname': userNickName}
			
	mongo.db.adminusers.replace_one({'id': id}, adminuser)
	return redirect(url_for('adminusers'))
	
@app.route("/adminusers/delete/<int:id>")
def adminusers_delete(id):
	facility = mongo.db.adminusers.delete_one({'id': id})
	return redirect(url_for('adminusers'))

@app.route("/transportation")
def transportation():
	return render_template('transportation.html')
	
def calendar_widget():
	#today = datetime.datetime.date(datetime.datetime.now())
	#current = re.split('-', str(today)) 
	#current_no = int(current[1]) 
	#current_month = year[current_no-1] 
	#current_day = int(re.sub('\A0', '', current[2])) 
	#current_yr = int(current[0])
	
	#print '%s %s' %(current_month, current_yr) 
	
	#print ''''''
	
	#if firstday == '0': print ''' SundayMondayTuesdayWednesdayThursdayFridaySaturday''' 
	#else: ## Here we assume a binary switch, a decision between '0' or not '0'; therefore, any non-zero argument will cause the calendar to start on Sunday. 
	#print '''MondayTuesdayWednesdayThursdayFridaySaturdaySunday'''SundayMondayTuesdayWednesdayThursdayFridaySaturday
	 
	#month = calendar.monthcalendar(current_yr, current_no) 
	#nweeks = len(month) 
	
	#for w in range(0,nweeks): 
	#week = month[w] 
	#print "" 
	#for x in xrange(0,7): 
	#day = week[x] 
	#if x == 5 or x == 6: 
	#classtype = 'weekend' 
	#else: 
	#classtype = 'day' 
 
	#if day == 0: 
	#classtype = 'previous' 
	#print '' %(classtype) 
	#elif day == current_day: 
	#print '%s' %(classtype, day, classtype) 
	#else: 
	#print '%s' %(classtype, day, classtype) 
	#print "" 
	#print ''' ''' 
	
	myCal = calendar.HTMLCalendar(calendar.SUNDAY)
	print(myCal.formatmonth(2009, 7))
	#return myCal.formatmonth(2009, 7)

	#return redirect(url_for('client_facility_registration', client_id=client_id ))
	
#app.jinja_env.globals.update(calendar_widget=calendar_widget)

if __name__ == "__main__":
    app.run(debug=True)
