import datetime
from flask_mongoengine import MongoEngine

db = MongoEngine()

class Facility(db.Document):
    fid = db.SequenceField()
    name = db.StringField(max_length=45, required=True)
    address = db.StringField(required=True)
    phonenumber = db.StringField(required=True)
    #csrf_token = db.StringField(required=True)
    created_date = db.DateTimeField(default=datetime.datetime.now)
    updated_date = db.DateTimeField(default=datetime.datetime.now)