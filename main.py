from flask import Flask, request, render_template
from flask_pymongo import PyMongo
import sys
#import pymongo
#from pymongo import MongoClient
#from flask_googlemaps import GoogleMaps

app = Flask(__name__)

#Database Settings

#{{googlemap("my_awesome_map", lat=0.23234234, lng=-0.234234234, markers=[(0.12, -0.45345)])}}

#MONGODB['server'] = '35.187.0.90'
#MONGODB['port'] = 27017
#MONGODB['database'] = 'passenger_dev'
#MONGODB[ 'username' ] = 'root'
#MONGODB[ 'password' ] = '6a7WAfZ1'


#app.config['MONGO_URI'] = 'mongodb://root:6a7WAfZ1@35.187.0.90:27017/passenger_dev'

app.config['MONGO_HOST'] = '35.187.0.90'
app.config['MONGO_PORT'] = '27017'
app.config['MONGO_DBNAME'] = 'admin'
app.config['MONGO_USERNAME'] = 'root'
app.config['MONGO_PASSWORD'] = '6a7WAfZ1'

#Google Map API Key
app.config['GOOGLEMAPS_KEY'] = "AIzaSyBgyhKX_x5nR3LXiDGKJg3OiyBAlsdbKaw"
#GoogleMaps(app)

mongo = PyMongo(app)

#connection = pymongo.MongoClient("35.187.0.90", 27017)
#db = connection['admin']
#db.authenticate("root", "6a7WAfZ1")

@app.route("/")
def hello():
	locations = mongo.db.testdb.find({})
	#return "Hello World!"
	return render_template('index.html', locations=locations)

@app.route("/admin/add/")
def admin_add():
	#online_users = mongo.db.users.find({})
	#return "Hello World!"
	return render_template('admin_add.html')
	
@app.route("/facility/")
def facility():
	#online_users = mongo.db.users.find({})
	#return "Hello World!"
	return render_template('facility.html')
	
@app.route("/facility/add/")
def facility_add():
	#online_users = mongo.db.users.find({})
	#return "Hello World!"
	return render_template('facility_add.html')

@app.route('/facility/add/', methods = ['POST'])
def facility_add_action():
   facilityName = request.form['facility_name']
   facilityAddress = request.form['facility_address']
   facilityPhoneNumber = request.form['facility_phonenumber']
   #print ("facilityName {}".format(facilityName))
   #print ("facilityAddress {}".format(facilityAddress))
   #print ("facilityPhoneNumber {0}".format(facilityPhoneNumber))
   #sys.exit(0)
   facility = {'name': facilityName, 'address': facilityAddress, 'phonenumber': facilityPhoneNumber}
   #facility.name = facilityName
   #facility.address = facilityAddress
   #facility.phonenumber = facilityPhoneNumber   
   mongo.db.facility.insert_one(facility)
   return render_template('facility_add.html')
	
@app.route('/admin/add/', methods = ['POST'])
def admin_form_|():
   location = request.form['location']
   mongo.db.testdb.insert_one({'location': location})
   return render_template('admin_add.html')

if __name__ == "__main__":
    app.run(debug=True)
